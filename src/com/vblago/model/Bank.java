package com.vblago.model;

public class Bank extends Credit implements Forward, Deposit, Exchange {
    public int year_licence;
    private final float USD_purchase = 27.05f;
    private final float USD_sale = 27.248f;

    private final float EUR_purchase = 31.8f;
    private final float EUR_sale = 32.15f;

    private final float RUR_purchase = 0.45f;
    private final float RUR_sale = 0.47f;

    public Bank(String name, String address, int year_licence) {
        super(name, address);
        this.year_licence = year_licence;
        this.percent = 0.25f;
    }

    @Override
    public float giveCredit(float UAH) {
        if (UAH <= 200_000) {
            return UAH * percent;
        }
        return -1;
    }

    @Override
    public float forwardMoney(float UAH) {
        return UAH * 1.01f + 5;
    }

    @Override
    public float giveDeposit(float UAH, int month) {
        if (month <= 12) {
            return UAH * 1.14f;
        }
        return 0;
    }

    @Override
    public float exchangeUSD(float note, boolean reverse) {
        if (!reverse) {
            return note / USD_sale;
        } else {
            return note * USD_purchase;
        }
    }

    @Override
    public float exchangeEUR(float note, boolean reverse) {
        if (!reverse) {
            return note / EUR_sale;
        } else {
            return note * EUR_purchase;
        }
    }

    @Override
    public float exchangeRUR(float note, boolean reverse) {
        if (!reverse) {
            return note / RUR_sale;
        } else {
            return note * RUR_purchase;
        }
    }
}
