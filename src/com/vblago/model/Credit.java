package com.vblago.model;

public abstract class Credit extends Organization {
    public float percent;

    public Credit(String name, String address) {
        super(name, address);
    }

    public abstract float giveCredit(float UAH);
}
