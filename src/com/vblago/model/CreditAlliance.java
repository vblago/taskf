package com.vblago.model;

public class CreditAlliance extends Credit{

    public CreditAlliance(String name, String address) {
        super(name, address);
        this.percent = 0.2f;
    }

    @Override
    public float giveCredit(float UAH) {
        if (UAH <= 100_000) {
            return UAH * percent;
        }
        return -1;
    }
}
