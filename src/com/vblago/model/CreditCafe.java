package com.vblago.model;

public class CreditCafe extends Credit {

    public CreditCafe(String name, String address) {
        super(name, address);
        this.percent = 2f;
    }

    @Override
    public float giveCredit(float UAH) {
        if (UAH <= 4_000) {
            return UAH * percent;
        }
        return -1;
    }
}
