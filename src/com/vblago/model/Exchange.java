package com.vblago.model;

public interface Exchange {
    float exchangeUSD(float note, boolean reverse);

    float exchangeEUR(float note, boolean reverse);

    float exchangeRUR(float note, boolean reverse);
}
