package com.vblago.model;

public class Exchanger extends Organization implements Exchange {
    private final float USD_purchase = 27.98f;
    private final float USD_sale = 27.13f;

    private final float EUR_purchase = 32.01f;
    private final float EUR_sale = 32.04f;

    public Exchanger(String name, String address) {
        super(name, address);
    }


    @Override
    public float exchangeUSD(float note, boolean reverse) {
        if (!reverse) {
            return note / USD_sale;
        } else {
            return note * USD_purchase;
        }
    }

    @Override
    public float exchangeEUR(float note, boolean reverse) {
        if (!reverse) {
            return note / EUR_sale;
        } else {
            return note * EUR_purchase;
        }
    }

    @Override
    public float exchangeRUR(float note, boolean reverse) {
        return 0;
    }
}
