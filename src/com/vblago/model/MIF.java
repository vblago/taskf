package com.vblago.model;

public class MIF extends Organization implements Deposit {
    public int year;

    public MIF(String name, String address, int year) {
        super(name, address);
        this.year = year;
    }

    @Override
    public float giveDeposit(float UAH, int month) {
        if (month >= 12){
            return UAH * 1.15f;
        }
        return 0;
    }

}
