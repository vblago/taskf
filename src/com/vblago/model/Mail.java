package com.vblago.model;

public class Mail extends Organization implements Forward{

    public Mail(String name, String address) {
        super(name, address);
    }

    @Override
    public float forwardMoney(float UAH) {
        return UAH * 1.02f;
    }
}
