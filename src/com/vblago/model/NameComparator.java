package com.vblago.model;

import java.util.Arrays;
import java.util.Comparator;

public class NameComparator implements Comparator<Organization> {
    @Override
    public int compare(Organization o1, Organization o2) {
        String[] nameOrg = {o1.name, o2.name};
        String[] resNameOrg = new String[nameOrg.length];
        System.arraycopy(nameOrg, 0, resNameOrg, 0, nameOrg.length);

        Arrays.sort(resNameOrg);
        if (nameOrg[0].equals(nameOrg[1])) {
            return 0;
        } else if (resNameOrg[0].equals(nameOrg[0])) {
            return -1;
        } else {
            return 1;
        }
    }
}
