package com.vblago.model;

public class Organization {
    public String name;
    public String address;

    public Organization(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
