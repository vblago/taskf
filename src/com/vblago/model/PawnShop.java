package com.vblago.model;

public class PawnShop extends Credit {

    public PawnShop(String name, String address) {
        super(name, address);
        this.percent = 0.4f;
    }

    @Override
    public float giveCredit(float UAH) {
        if (UAH <= 50_000) {
            return UAH * percent;
        }
        return -1;
    }
}

