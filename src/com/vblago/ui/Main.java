package com.vblago.ui;

import com.vblago.model.Credit;
import com.vblago.model.NameComparator;
import com.vblago.model.Organization;
import com.vblago.util.Generator;
import com.vblago.util.Operations;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<Organization> organizations = Generator.getOrganizations();

        Collections.sort(organizations, new NameComparator());
        for (Organization organization : organizations){
            System.out.println(organization.name);
        }

        Organization smallestPercentOrganization = Operations.findSmallestPercent(organizations, Float.parseFloat(scan.nextLine()));
        if (smallestPercentOrganization != null) {
            System.out.println(String.format("Минимальная процентная ставка составляет: %.2f%%", ((Credit) smallestPercentOrganization).percent));
        } else {
            System.out.println("Организации, которая даст кредит на такую сумму, нет");
        }

        Organization bestForward = Operations.findBestForward(organizations, Float.parseFloat(scan.nextLine()));
        if (bestForward != null) {
            System.out.println(String.format("Лучшая организация для перессылки денег это %s", bestForward.name));
        } else {
            System.out.println("Организации, которая перешлет деньги, нет");
        }

        Organization bestDeposit = Operations.findBestDeposit(organizations, Float.parseFloat(scan.nextLine()), Integer.parseInt(scan.nextLine()));
        if (bestDeposit != null) {
            System.out.println(String.format("Лучшая организация для дерозита это %s", bestDeposit.name));
        } else {
            System.out.println("Организации, которая даст депозит, нет");
        }

        System.out.println("Введите сумму, название входной банкноты, название выходной банкноты");
        float note = Float.parseFloat(scan.nextLine());
        String inNote = scan.nextLine();
        String outNote = scan.nextLine();
        Organization bestExchange = Operations.findBestExchange(organizations, note, inNote, outNote);
        if (bestExchange != null) {
            System.out.println(String.format("Лучший обмен валюты это %.2f", Operations.doExchange(bestExchange, note, inNote, outNote)));
        } else {
            System.out.println("Организации, которая обменяет деньги, нет");
        }

    }
}
