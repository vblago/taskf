package com.vblago.util;

import com.vblago.model.*;

import java.util.ArrayList;
import java.util.List;

public class Generator {
    public static List<Organization> getOrganizations() {
        List<Organization> organizations = new ArrayList<>();

        organizations.add(new PawnShop("Благо", "Шевченко 1б"));
        organizations.add(new CreditCafe("ШвидкоЗайм", "Героев труда 56"));
        organizations.add(new CreditAlliance("KCD", "Казимирова 23"));
        organizations.add(new Bank("Приват", "Сумская 5", 2005));
        organizations.add(new Mail("Нова Пошта", "Валентиновская 27"));
        organizations.add(new MIF("Стабильность", "Студенческая 2", 1998));
        organizations.add(new Exchanger("Обменка", "Вавилова 5"));
        return organizations;
    }
}
