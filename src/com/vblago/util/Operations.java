package com.vblago.util;

import com.vblago.model.*;

import java.util.List;

public class Operations {

    public static Organization findSmallestPercent(List<Organization> organizations, float UAH) {
        int q = 0;
        float min = -1;
        Organization minOrganization = null;
        for (Organization organization : organizations) {
            if (organization instanceof Credit) {
                min = ((Credit) organization).giveCredit(UAH);
                if (min >= 0) {
                    minOrganization = organization;
                    break;
                }
            }
            q++;
        }

        for (int i = q + 1; i < organizations.size(); i++) {
            if (organizations.get(i) instanceof Credit) {
                float res = ((Credit) organizations.get(i)).giveCredit(UAH);
                if ((min > res) && (res >= 0)) {
                    minOrganization = organizations.get(i);
                    min = res;
                }
            }
        }
        return minOrganization;
    }

    public static Organization findBestForward(List<Organization> organizations, float UAH) {
        int q = 0;
        float min = -1;
        Organization minOrganization = null;
        for (Organization organization : organizations) {
            if (organization instanceof Forward) {
                min = ((Forward) organization).forwardMoney(UAH);
                if (min >= 0) {
                    minOrganization = organization;
                    break;
                }
            }
            q++;
        }

        for (int i = q + 1; i < organizations.size(); i++) {
            if (organizations.get(i) instanceof Forward) {
                float res = ((Forward) organizations.get(i)).forwardMoney(UAH);
                if ((min > res) && (res >= 0)) {
                    minOrganization = organizations.get(i);
                    min = res;
                }
            }
        }
        return minOrganization;
    }

    public static Organization findBestDeposit(List<Organization> organizations, float UAH, int month) {
        float max = 0;
        Organization maxOrganization = null;

        for (Organization organization : organizations) {
            if (organization instanceof Deposit) {
                float res = ((Deposit) organization).giveDeposit(UAH, month);
                if (max < res) {
                    maxOrganization = organization;
                    max = res;
                }
            }
        }
        return maxOrganization;
    }

    public static Organization findBestExchange(List<Organization> organizations, float note, String inNote, String outNote) {
        float max = 0;
        Organization maxOrganization = null;

        for (Organization organization : organizations) {
            if (organization instanceof Exchange) {
                float res = doExchange(organization, note, inNote, outNote);
                if (max < res) {
                    maxOrganization = organization;
                    max = res;
                }
            }
        }
        return maxOrganization;
    }

    public static float doExchange(Organization organization, float note, String inNote, String outNote) {
        float result = 0;
        if (inNote.equalsIgnoreCase("UAH")) {
            if (outNote.equalsIgnoreCase("USD")) {
                result = ((Exchange) organization).exchangeUSD(note, false);
            } else if (outNote.equalsIgnoreCase("EUR")) {
                result = ((Exchange) organization).exchangeEUR(note, false);
            } else if (outNote.equalsIgnoreCase("RUR")) {
                result = ((Exchange) organization).exchangeRUR(note, false);
            }
        } else {
            if (inNote.equalsIgnoreCase("USD")) {
                result = ((Exchange) organization).exchangeUSD(note, true);
            } else if (inNote.equalsIgnoreCase("EUR")) {
                result = ((Exchange) organization).exchangeEUR(note, true);
            } else if (inNote.equalsIgnoreCase("RUR")) {
                result = ((Exchange) organization).exchangeRUR(note, true);
            }
        }
        return result;
    }
}
